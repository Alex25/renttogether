Here are five scenarios to try the app by ourselves (if you want to redo the scenarios, please empty the app's cache)



**Scenario 1 :  John and his girlfriend want to go from Berlin to Munich at 10 am on the 02/16/2017**

Go to the “Book a new trip” page on the left menu.

Enter Berlin as departure and Munich as arrival.

Select 02/16/2017 for the date.

Select 2 people.

Select 10 am.

Click on “submit”.

You can move between the different tabs to see the result for bus, train and plane.

On the bus tab, use the “earlier” and “later” button to find a trip offer by RentTogether.

Click on it and follow the different steps to book the trip.




**Scenario 2 : John wants to go to Black Forest on Saturday, 18th February or Sunday, 19th of February with 6 of his friends. He begins his research for Saturday. Unfortunately there is no result but John is encouraged to post a message into the forum to discuss with possible other interested users.**

On the left menu, go to “Find Travel Buddies” page (the forum).

Read the posts and find the one about a trip to Black Forest. Click on it (you should get a message saying that you have clicked). Enter a comments saying that you and six friends are interested in this trip. Wait a few seconds. 

A forum administrator may see that there are now enough people for this trip and send you a notification. Read the notification. 

Go to the “My trip” page on the left menu and to the “suggested” tab as said in the notification. Then click on the trip and follow the steps to book it.




**Scenario 3 : John is a hiking fan. He wants to go to Garmisch-Partenkirchen the next weekend (02/18).**

Enter Munich as departure point and  Garmisch-Partenkirchen as arrival point.

Select 02/18/2017 for the date.

Select the number of people you want and time you want.

Submit your request.

No result ? Go to the “Offers and trending trips” page on the left menu. On the “trending” trips tab, you can see that your trip is there. Click on it and see what happens. 




**Scenario 4 : John is a fan of ski and he is looking for a trip**

Go to the “Offers and trending trips” page on the left menu. On the “trending” trips tab, you can see a trip to Winterberg. 

Click on it to say that you are interested. Go to the “My trips” page on the left menu. Under the “suggested” tab you should see the trip now available for booking (since there is no people missing anymore). 

Click on it and follow the different steps to book it.




**Scenario 5 : John doesn’t really know where he wants to go but he has two weeks of vacation and wants to travel.**

Go to the “Offers and trending trips” page on the left menu. Go to the “trending“ tab.

Click on the trip to Feldberg and see what happens.

Then go to the “offers” tab. Follow the different steps to book the trip to Aachen.