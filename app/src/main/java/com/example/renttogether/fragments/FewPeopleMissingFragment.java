package com.example.renttogether.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.renttogether.R;
import com.example.renttogether.activities.CustomListAdapterMissingPeople;
import com.example.renttogether.database.DatabaseHandler;
import com.example.renttogether.database.TripsDB;

import java.util.ArrayList;


public class FewPeopleMissingFragment extends ListFragment {

    //Adapter to handle data of list view
    CustomListAdapterMissingPeople adapter;

    //List of all booked trips
    ArrayList<TripsDB> standbyTrips = new ArrayList<>();

    ListView lv;

    public FewPeopleMissingFragment(){
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_few_people_missing, container, false);
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        // save views as variables in this method
        // "view" is the one returned from onCreateView

        /* Define list view */
        lv = (ListView) view.findViewById(android.R.id.list);

        /* Get accurate trips from DB */
        DatabaseHandler db = new DatabaseHandler(getContext());

        standbyTrips = db.getTripsByState("stand-by");

        /* Set custom adapter */
        adapter = new CustomListAdapterMissingPeople(getContext(), standbyTrips);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {
                TripsDB trip = (TripsDB) adapter.getItemAtPosition(position);
                if(trip.getid() == 18) { // Munich-Garmisch scenario 3
                    Toast.makeText(getContext(), "Your interest for this trip has already been taken into account !", Toast.LENGTH_LONG).show();
                }
                else {
                    if(trip.getid() == 20) {  //Munich-Winterberg scenario 3
                        /* Update trip' state attribute in DB */
                        DatabaseHandler db = new DatabaseHandler(getContext());
                        TripsDB selectedTrip = db.getTripByID(trip.getid());
                        selectedTrip.setState("suggested");
                        db.updateTrip(selectedTrip);
                        Toast.makeText(getContext(), "This trip is now available in your suggested trips !", Toast.LENGTH_LONG).show();
                    } else {
                        if(trip.getid() == 21){ //Munich-Feldberg scenario 4
                            /* Update trip' state attribute in DB */
                            DatabaseHandler db = new DatabaseHandler(getContext());
                            TripsDB selectedTrip = db.getTripByID(trip.getid());
                            if(selectedTrip.getNbrOfPeople().equals("4")) {
                                selectedTrip.setNbrOfPeople("3");
                                db.updateTrip(selectedTrip);
                                Toast.makeText(getContext(), "Thank you, your interest for this trip has been taken into account",
                                        Toast.LENGTH_LONG).show();
                            } else {
                                if(selectedTrip.getNbrOfPeople().equals("3")) {
                                    Toast.makeText(getContext(), "Your interest for this trip has already been taken into account",
                                            Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    }

                    // Reload current fragment
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.detach(FewPeopleMissingFragment.this).attach(FewPeopleMissingFragment.this).commit();
                }
            }
        });
    }
}
