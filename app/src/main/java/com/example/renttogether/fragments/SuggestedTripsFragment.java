package com.example.renttogether.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.example.renttogether.R;
import com.example.renttogether.activities.CustomListAdapterSuggestedTrips;
import com.example.renttogether.database.DatabaseHandler;
import com.example.renttogether.database.TripsDB;

import java.util.ArrayList;

public class SuggestedTripsFragment extends ListFragment implements NumberPicker.OnValueChangeListener {

    //Adapter to handle data of list view
    CustomListAdapterSuggestedTrips adapter;

    //List of all booked trips
    ArrayList<TripsDB> suggestedTrips = new ArrayList<>();

    ListView lv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_suggested_trips, container, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        // save views as variables in this method
        // "view" is the one returned from onCreateView

        /* Define list view */
        lv = (ListView) view.findViewById(android.R.id.list);

        /* Get accurate trips from DB */
        DatabaseHandler db = new DatabaseHandler(getContext());

        suggestedTrips = db.getTripsByState("suggested");

        /* Set custom adapter */
        adapter = new CustomListAdapterSuggestedTrips(getContext(), suggestedTrips);
        lv.setAdapter(adapter);


        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {
                TripsDB trip = (TripsDB) adapter.getItemAtPosition(position);
                showNbrPeople(trip);
            }

        });
    }

    public void showNbrPeople(final TripsDB trip) {
        final AlertDialog d = new AlertDialog.Builder(getActivity())
                .setTitle("Select number of people")
                .setView(R.layout.nbr_people_dialog)
                .create();
        d.show();
        Button set_button = (Button) d.findViewById(R.id.set_button);
        final NumberPicker np = (NumberPicker) d.findViewById(R.id.numberPicker);
        np.setMaxValue(100);
        np.setMinValue(1);
        np.setWrapSelectorWheel(false);
        np.setOnValueChangedListener(this);
        set_button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                String nbrOfPeople = String.valueOf(np.getValue());
                PaymentDialog dialog = new PaymentDialog(getActivity(), trip.getPrice(), trip.getid(), nbrOfPeople);
                dialog.setCancelable(false);
                dialog.show();

                TextView textView = (TextView) dialog.findViewById(R.id.trip_summary);
                textView.setText("\n\n\n" + "* " + trip.getDeparture() + "-" + trip.getArrival() + "\n\n"
                        + "* On " + trip.getDate() + "\n\n" + "* At " + trip.getTime() + "\n\n"
                        + "* " + nbrOfPeople + "x" + trip.getPrice() + "\n\n"
                        + "* By " + trip.getMeansOfTransport() + "\n\n"
                        + "* Duration : " + trip.getDuration() + "\n\n" + "* " + trip.getConnection());
                d.dismiss();
            }
        });
        d.show();
    }

    @Override
    public void onValueChange(NumberPicker numberPicker, int i, int i1) {

    }
}
