package com.example.renttogether.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.example.renttogether.R;
import com.example.renttogether.activities.PaymentActivity;

/**
 * Created by Alexandra GAUDRON on 14/01/2017.
 */
public class PaymentDialog extends Dialog implements
        android.view.View.OnClickListener {

    private Activity c;
    private Button yes, no;
    private String trip_price;
    private int trip_id;
    private String nbrOfPeople;

    final int sdk = android.os.Build.VERSION.SDK_INT;


    public PaymentDialog(Activity a, String trip_price, int trip_id, String nbrOfPeople) {
        // isNbrOfPeople = true if coming from NewTripActivity, false otherwise
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.trip_price = trip_price;
        this.trip_id = trip_id;
        this.nbrOfPeople = nbrOfPeople;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.payment_dialog);
        yes = (Button) findViewById(R.id.btn_yes);
        no = (Button) findViewById(R.id.btn_no);
        yes.setOnClickListener(this);
        no.setOnClickListener(this);

       /* Background picture settings */
        if(sdk > Build.VERSION_CODES.KITKAT) {  // Not sure of the version to put
            Drawable map_background = ContextCompat.getDrawable(this.getContext(), R.drawable.road_background_light);
        }
        else {
            Drawable map_background = ContextCompat.getDrawable(this.getContext(), R.drawable.road_background);
            map_background.setAlpha(60); // 0 full transparency, 255 full opacity
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_yes:
                Intent i = new Intent(c, PaymentActivity.class);
                Bundle extras = new Bundle();
                extras.putString("PRICE",trip_price);
                extras.putInt("ID", trip_id);
                extras.putString("NBR", nbrOfPeople);
                i.putExtras(extras);
                c.startActivity(i);
                //c.finish();
                break;
            case R.id.btn_no:
                dismiss();
                break;
            default:
                break;
        }
        dismiss();
    }
}