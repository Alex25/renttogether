package com.example.renttogether.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.CursorIndexOutOfBoundsException;
import android.net.Uri;
import android.support.v4.app.ListFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.renttogether.activities.NewTripActivity;
import com.example.renttogether.database.DatabaseHandler;
import com.example.renttogether.R;
import com.example.renttogether.database.TripsDB;
import com.example.renttogether.activities.CustomListAdapter;
import com.example.renttogether.activities.TripResultActivity;

import java.util.ArrayList;


public class BusFragment extends ListFragment {

    //List of all trips for day query
    ArrayList<TripsDB> resultTotalTrips = new ArrayList<>();

    //List of trips currently displayed
    ArrayList<TripsDB> resultTrips = new ArrayList<>();

    //Adapter to handle data of list view
    CustomListAdapter adapter;

    ListView lv;

    static Context context;
    static Activity activity;

    static AlertDialog.Builder alertbox;
    static AlertDialog alertDialog;



    public BusFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getContext();
        activity = getActivity();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_transport, container, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        // save views as variables in this method
        // "view" is the one returned from onCreateView

        /* Define list view */
        lv = (ListView) view.findViewById(R.id.list);

        /* Get accurate trips from DB */
        DatabaseHandler db = new DatabaseHandler(getContext());

        resultTotalTrips = db.getTripByDepartureAndArrivalAndMeansOfTransportAndDate(TripResultActivity.getDeparturePoint(),
                TripResultActivity.getArrivalPoint(), TripResultActivity.getDateTrip(),"bus");
        //Toast.makeText(getContext(), DatabaseHandler.tripsToString((resultTrips)), Toast.LENGTH_LONG).show();

        resultTrips = displayFourGoodTrips(resultTotalTrips, TripResultActivity.getTimeTrip());




        /* Set custom adapter */
        adapter = new CustomListAdapter(getContext(), resultTrips);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {
                TripsDB trip = (TripsDB) adapter.getItemAtPosition(position);
                String url = trip.getUrl();
                if(url!=null) { //external company
                    Uri uri = Uri.parse(url); // missing 'http://' will cause crashed
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }
                else {
                    PaymentDialog dialog = new PaymentDialog(getActivity(), trip.getPrice(), trip.getid(), TripResultActivity.getSetPeopleNbr());
                    dialog.setCancelable(false);
                    dialog.show();

                    TextView textView = (TextView) dialog.findViewById(R.id.trip_summary);
                    textView.setText("\n\n\n" + "* " + trip.getDeparture() + "-" + trip.getArrival() + "\n\n"
                            + "* On " + trip.getDate() + "\n\n" + "* At " + trip.getTime() + "\n\n"
                            + "* " + TripResultActivity.getSetPeopleNbr() + "x" + trip.getPrice() + "\n\n"
                            + "* By " + trip.getMeansOfTransport() + "\n\n"
                            + "* Duration : " + trip.getDuration() + "\n\n" + "* " + trip.getConnection());
                }
            }
        });

        /* Previous button */
        Button previous_button = (Button) view.findViewById(R.id.previousTrips);
        previous_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (resultTotalTrips.size() != 0){
                    if (displayPreviousFourGoodTrips(resultTotalTrips, resultTrips).size() != 0) {
                        resultTrips = displayPreviousFourGoodTrips(resultTotalTrips, resultTrips);

                        adapter = new CustomListAdapter(getContext(), resultTrips);
                        lv.setAdapter(adapter);

                        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {
                                TripsDB trip = (TripsDB) adapter.getItemAtPosition(position);
                                String url = trip.getUrl();
                                if (url != null) {
                                    Uri uri = Uri.parse(url); // missing 'http://' will cause crashed
                                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                    startActivity(intent);
                                }
                                else {
                                    PaymentDialog dialog = new PaymentDialog(getActivity(), trip.getPrice(), trip.getid(), TripResultActivity.getSetPeopleNbr());
                                    dialog.setCancelable(false);
                                    dialog.show();

                                    TextView textView = (TextView) dialog.findViewById(R.id.trip_summary);
                                    textView.setText("\n\n\n" + "* " + trip.getDeparture() + "-" + trip.getArrival() + "\n\n"
                                            + "* On " + trip.getDate() + "\n\n" + "* At " + trip.getTime() + "\n\n"
                                            + "* " + TripResultActivity.getSetPeopleNbr() + "x" + trip.getPrice() + "\n\n"
                                            + "* By " + trip.getMeansOfTransport() + "\n\n"
                                            + "* Duration : " + trip.getDuration() + "\n\n" + "* " + trip.getConnection());
                                }
                            }
                        });
                    } else {
                        Toast.makeText(getContext(), "No earlier trip available", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getContext(), "No earlier trip available", Toast.LENGTH_LONG).show();
                }
            }
        });

        /* Next button */
        Button next_button = (Button) view.findViewById(R.id.nextTrips);
        next_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if (resultTotalTrips.size() != 0) {
                    if (displayNextFourGoodTrips(resultTotalTrips, resultTrips).size() != 0) {
                        resultTrips = displayNextFourGoodTrips(resultTotalTrips, resultTrips);

                        adapter = new CustomListAdapter(getContext(), resultTrips);
                        lv.setAdapter(adapter);

                        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {
                                TripsDB trip = (TripsDB) adapter.getItemAtPosition(position);
                                String url = trip.getUrl();
                                if (url != null) {
                                    Uri uri = Uri.parse(url); // missing 'http://' will cause crashed
                                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                    startActivity(intent);
                                }
                                else {
                                    PaymentDialog dialog = new PaymentDialog(getActivity(), trip.getPrice(), trip.getid(), TripResultActivity.getSetPeopleNbr());
                                    dialog.setCancelable(false);
                                    dialog.show();

                                    TextView textView = (TextView) dialog.findViewById(R.id.trip_summary);
                                    textView.setText("\n\n\n" + "* " + trip.getDeparture() + "-" + trip.getArrival() + "\n\n"
                                            + "* On " + trip.getDate() + "\n\n" + "* At " + trip.getTime() + "\n\n"
                                            + "* " + TripResultActivity.getSetPeopleNbr() + "x" + trip.getPrice() + "\n\n"
                                            + "* By " + trip.getMeansOfTransport() + "\n\n"
                                            + "* Duration : " + trip.getDuration() + "\n\n" + "* " + trip.getConnection());
                                }
                            }
                        });
                    } else {
                        Toast.makeText(getContext(), "No later trip available", Toast.LENGTH_LONG).show();
                    }
                }else {
                    Toast.makeText(getContext(), "No later trip available", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    /* public void addItems(String text) {
        listItems.add(text);
        adapter.notifyDataSetChanged();
     }*/

    public void removeAllItems(ArrayList<TripsDB> listItems, ArrayAdapter adapter) {
        for(int i=0; i<listItems.size(); i++) {
            listItems.remove(i);
            adapter.notifyDataSetChanged();
        }
    }

    /* Display four trips corresponding to user request : with time equal or sup. to user specified time */
    public static ArrayList<TripsDB> displayFourGoodTrips(ArrayList<TripsDB> trips, String time_str){
        ArrayList<TripsDB> res = new ArrayList<>();
        int index = 0;
        boolean existTimeSup = false; // is there a trip in DB which time is superior than time specified by user
        trips = orderArray(trips);
        /*for(TripsDB trip : trips){
            Log.d("TAG", trip.getTime());
        }*/
        int time_int = timeStrToInt(time_str);
        for(TripsDB trip : trips){
            if(timeStrToInt(trip.getTime()) >= time_int){
                index = trips.indexOf(trip);
                res.add(trip);
                existTimeSup = true;
                break;
            }
        }
        // At least one trip in DB scheduled on/after time set by user
        if(existTimeSup) {
            for (int i = 1; i < 4; i++) {
                if (index + i < trips.size()) { //index corresponds to trip with the time bigger and the closest to time set by user
                    res.add(trips.get(index + i));
                }
            }
        }
        else {
            //showAlertDialog("Don't forget to...", "Have a look at the special offers and discuss your trip on the forum", context);

            // No trips available after time set by user, so display the later 4 before time set
            for(int i=4; i>0; i--) {
                if(trips.size() - i >= 0) {
                    res.add(trips.get(trips.size() - i));
                }
            }
        }
        return res;
    }

    /* Display next four trips (if possible) when user clicks on next button */
    public static ArrayList<TripsDB> displayNextFourGoodTrips(ArrayList<TripsDB> totalTrips, ArrayList<TripsDB> displayedTrips){
        //Log.d("TAG", "Start of next method, index : " + String.valueOf(index));
        //Log.d("TAG", String.valueOf(trips.size()));
        ArrayList<TripsDB> res = new ArrayList<>();
        int index = getIndexOfTrip(totalTrips, displayedTrips.get(displayedTrips.size()-1));
        Log.d("TAG", "Start of next method, index : " + String.valueOf(index));
        for(int i=1; i<5; i++){
            if(index + i < totalTrips.size()){
                res.add(totalTrips.get(index + i));
            }
        }
        index = index + res.size();
        Log.d("TAG", "After next : " + String.valueOf(index));

        return res;
    }

    /* Display four previous trips when user click on previous button */
    public static ArrayList<TripsDB> displayPreviousFourGoodTrips(ArrayList<TripsDB> totalTrips, ArrayList<TripsDB> displayedTrips){
        //Log.d("TAG", "Start of previous method, index : " + String.valueOf(index));
        ArrayList<TripsDB> res = new ArrayList<>();
        int index;

        try {
            index = getIndexOfTrip(totalTrips, displayedTrips.get(0));
        } catch (IndexOutOfBoundsException err){
            err.printStackTrace();
            index = totalTrips.size();
        }

        Log.d("TAG", "Start of previous method, index : " + String.valueOf(index));
        for(int i=1; i<5; i++){
            if(index - i >= 0){
                res.add(totalTrips.get(index - i));
            }
        }
        res = orderArray(res);
        index = index - res.size();
        Log.d("TAG", "After previous : " + String.valueOf(index));


        return res;
    }

    /*Return trips scheduled on time or after time det by user */
    public static ArrayList<TripsDB> getTripsGoodTime(ArrayList<TripsDB> trips, String time_str){
        ArrayList<TripsDB> res = new ArrayList<>();
        int time_int = timeStrToInt(time_str);
        for(TripsDB trip : trips){
            if(timeStrToInt(trip.getTime()) >= time_int){
                res.add(trip);
            }

            res = orderArray(res);
        }
        return res;
    }

    /* Convert string time of TripDB (..h..) to int */
    public static int timeStrToInt(String time_str){
        return Integer.parseInt(time_str.substring(0,2))*60 + Integer.parseInt(time_str.substring(3,5));
    }

    /* Order array by time of TripDB */
    public static ArrayList<TripsDB> orderArray(ArrayList<TripsDB> trips) {
        TripsDB temp;
        for (int i = 1; i < trips.size(); i++) {
            for (int j = 0; j < trips.size() - i; j++) {
                if (timeStrToInt(trips.get(j).getTime()) > timeStrToInt(trips.get(j + 1).getTime())) {
                    temp = trips.get(j);
                    trips.set(j, trips.get(j + 1));
                    trips.set(j + 1, temp);
                }
            }
        }
        return trips;
    }

    public static int getIndexOfTrip(ArrayList<TripsDB> trips, TripsDB selected_trip){
        int index = 0;
        for (int i=0; i<trips.size(); i++){
            if((trips.get(i).getid())== (selected_trip.getid())){
                index = i;
                break;
            }
        }
        return index;

    }

    /* Display or not the dialog for forum and special offers */
    public static void showAlertDialog(final String title, String message,
                                       final Context context) {
        if (alertDialog != null && alertDialog.isShowing()) {
            // A dialog is already open, wait for it to be dismissed, do nothing
        } else {
            alertbox = new AlertDialog.Builder(context);
            alertbox.setTitle(title);
            alertbox.setMessage(message);
            alertbox.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    alertDialog.dismiss();
                }
            });

            alertDialog = alertbox.create();
            alertDialog.show();
        }
    }

}
