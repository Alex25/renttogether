package com.example.renttogether.fragments;

/**
 * Created by rclakmal on 1/29/17.
 */

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.ListFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.renttogether.R;
import com.example.renttogether.activities.FirebaseThreadViewHolder;
import com.example.renttogether.database.Thread;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;


public class NewThreadMessageFragment extends ListFragment {
    private DatabaseReference mThreadReference;
    //Adapter to handle data of list view
    FirebaseThreadViewHolder adapter;
    private FirebaseRecyclerAdapter mFirebaseAdapter;
    RecyclerView mThreadsRecyclerView;
    ArrayList<Thread> messages = new ArrayList<>();
    LinearLayoutManager layoutManager;
    RecyclerView lv;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final FragmentActivity c = getActivity();
        layoutManager = new LinearLayoutManager(c);

        View mainView =  inflater.inflate(R.layout.fragment_thread_detail, container, false);
        return mainView;
    }


    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {

    }


}




