package com.example.renttogether.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.renttogether.R;
import com.example.renttogether.activities.TripResultActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class NewTripFragment extends Fragment implements NumberPicker.OnValueChangeListener {

    /** Private members of the class for date and time */
    private TextView pDisplayDate;
    private TextView pDisplayTime;
    SimpleDateFormat dfDate = new SimpleDateFormat();
    SimpleDateFormat dfTime = new SimpleDateFormat();

    private String departure = null;
    private String arrival = null;

    /** Private members of the class for number of people */
    private TextView pDisplayNbrPeople;

    final int sdk = android.os.Build.VERSION.SDK_INT;

    @Override
    public void onStart() {
        super.onStart();

        /* Background picture settings */
        if(sdk > Build.VERSION_CODES.KITKAT) {  // Not sure of the version to put
            Drawable map_background = ContextCompat.getDrawable(this.getContext(), R.drawable.road_background_light);
        }
        else {
            Drawable map_background = ContextCompat.getDrawable(this.getContext(), R.drawable.road_background);
            map_background.setAlpha(60); // 0 full transparency, 255 full opacity
        }

        /* AutoCompleteTextView for departure and arrival points */
        AutoCompleteTextView departure_tv = (AutoCompleteTextView) getView().findViewById(R.id.departure_tv);
        AutoCompleteTextView arrival_spinner = (AutoCompleteTextView) getView().findViewById(R.id.arrival_tv);

        ArrayAdapter<CharSequence> new_adapter = ArrayAdapter.createFromResource(this.getContext(),
                R.array.cities_array, R.layout.my_autocomplete_item);

        departure_tv.setAdapter(new_adapter);
        arrival_spinner.setAdapter(new_adapter);




        /* Select number of people */
        pDisplayNbrPeople = (EditText) getView().findViewById(R.id.nbr_people);
        if(sdk <= Build.VERSION_CODES.KITKAT) {  // Not sure of the version to put
            Drawable mDrawable = getContext().getResources().getDrawable(R.drawable.ic_people);
            mDrawable.setColorFilter(new PorterDuffColorFilter(0x125688, PorterDuff.Mode.MULTIPLY));
            ImageView people_iv = (ImageView) getView().findViewById(R.id.people_iv);
            people_iv.setImageDrawable(mDrawable);

            ImageView imgView = (ImageView)getView().findViewById(R.id.nameIcon1);
            imgView.setVisibility(View.GONE);
        }

        pDisplayNbrPeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showNbrPeople();
            }
        });




        /* Date and time elements */
        /** Capture our View elements */
        pDisplayDate = (EditText) getView().findViewById(R.id.displayDate);
        if(sdk <= Build.VERSION_CODES.KITKAT) {  // Not sure of the version to put
            Drawable mDrawableDate = getContext().getResources().getDrawable(R.drawable.ic_calendar);
            mDrawableDate.setColorFilter(new PorterDuffColorFilter(0x125688, PorterDuff.Mode.MULTIPLY));
            ImageView date_iv = (ImageView) getView().findViewById(R.id.date_iv);
            date_iv.setImageDrawable(mDrawableDate);

            ImageView imgView = (ImageView)getView().findViewById(R.id.nameIcon2);
            imgView.setVisibility(View.GONE);
        }

        pDisplayDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerFragment dpf = new DatePickerFragment();
                dpf.setCallBack(onDate);
                dpf.show(getFragmentManager().beginTransaction(), "DatePickerFragment");
            }
        });



        pDisplayTime = (EditText) getView().findViewById(R.id.displayTime);
        if(sdk <= Build.VERSION_CODES.KITKAT) {  // Not sure of the version to put
            Drawable mDrawableTime = getContext().getResources().getDrawable(R.drawable.ic_time);
            mDrawableTime.setColorFilter(new PorterDuffColorFilter(0x125688, PorterDuff.Mode.MULTIPLY));
            ImageView time_iv = (ImageView) getView().findViewById(R.id.time_iv);
            time_iv.setImageDrawable(mDrawableTime);

            ImageView imgView = (ImageView)getView().findViewById(R.id.nameIcon3);
            imgView.setVisibility(View.GONE);
        }

        pDisplayTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerFragment dpf = new TimePickerFragment();
                dpf.setCallBack(onTime);
                dpf.show(getFragmentManager().beginTransaction(), "TimePickerFragment");
            }
        });


        /** Get the current date/time */
        final Calendar cal = Calendar.getInstance();
        dfDate.applyPattern("MM/dd/yyyy");
        dfTime.applyPattern("HH:mm");

        pDisplayDate.setText(dfDate.format(cal.getTime()));
        pDisplayTime.setText(dfTime.format(cal.getTime()));

        /* Start search */
        Button search_button = (Button) getView().findViewById(R.id.search_button);
        search_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                /* Retrieve departure and arrival points */
                AutoCompleteTextView departure_tv = (AutoCompleteTextView) getView().findViewById(R.id.departure_tv);
                departure = departure_tv.getText().toString();
                AutoCompleteTextView arrival_tv = (AutoCompleteTextView) getView().findViewById(R.id.arrival_tv);
                arrival = arrival_tv.getText().toString();

                Intent intent = new Intent(getActivity().getApplicationContext(), TripResultActivity.class);
                Bundle extras = new Bundle();
                extras.putString("DEPARTURE",departure);
                extras.putString("ARRIVAL",arrival);
                extras.putString("DATE", pDisplayDate.getText().toString());
                extras.putString("TIME", pDisplayTime.getText().toString());
                extras.putString("NBR_SET", pDisplayNbrPeople.getText().toString());
                intent.putExtras(extras);
                startActivity(intent);

            }
        });
    }


    /* Methods of dialog for number of people */
    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        Log.i("value is",""+ newVal);
    }

    public void showNbrPeople()
    {
        final AlertDialog d = new AlertDialog.Builder(getActivity())
                .setTitle("Select number of people")
                .setView(R.layout.nbr_people_dialog)
                .create();
        d.show();
        Button set_button = (Button) d.findViewById(R.id.set_button);
        final NumberPicker np = (NumberPicker) d.findViewById(R.id.numberPicker);
        np.setMaxValue(100);
        np.setMinValue(1);
        np.setWrapSelectorWheel(false);
        np.setOnValueChangedListener(this);
        set_button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                pDisplayNbrPeople.setText(String.valueOf(np.getValue()));
                d.dismiss();
            }
        });
        d.show();
    }

    DatePickerDialog.OnDateSetListener onDate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            GregorianCalendar date = new GregorianCalendar(year, monthOfYear, dayOfMonth);
            pDisplayDate.setText(dfDate.format(date.getTime()));
        }
    };

    TimePickerDialog.OnTimeSetListener onTime = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int hour, int minute) {
            GregorianCalendar date = new GregorianCalendar();
            date.set(Calendar.HOUR_OF_DAY, hour);
            date.set(Calendar.MINUTE, minute);
            pDisplayTime.setText(dfTime.format(date.getTime()));
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.new_trip_fragment, container, false);
    }
}
