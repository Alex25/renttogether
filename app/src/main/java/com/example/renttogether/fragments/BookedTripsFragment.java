package com.example.renttogether.fragments;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.renttogether.R;
import com.example.renttogether.activities.CustomListAdapterBookedTrips;
import com.example.renttogether.database.DatabaseHandler;
import com.example.renttogether.database.TripsDB;

import java.util.ArrayList;

public class BookedTripsFragment extends ListFragment {

    //Adapter to handle data of list view
    CustomListAdapterBookedTrips adapter;

    //List of all booked trips
    ArrayList<TripsDB> bookedTrips = new ArrayList<>();

    ListView lv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_booked_trips, container, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        // save views as variables in this method
        // "view" is the one returned from onCreateView

        /* Define list view */
        lv = (ListView) view.findViewById(android.R.id.list);

        /* Get accurate trips from DB */
        DatabaseHandler db = new DatabaseHandler(getContext());

        bookedTrips = db.getTripsByState("booked");

        /* Set custom adapter */
        adapter = new CustomListAdapterBookedTrips(getContext(), bookedTrips);
        lv.setAdapter(adapter);
}

}
