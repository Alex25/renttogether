package com.example.renttogether.fragments;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.renttogether.R;
import com.example.renttogether.activities.FirebaseThreadViewHolder;
import com.example.renttogether.database.Thread;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.Bind;

import static android.content.Context.NOTIFICATION_SERVICE;
import static java.lang.System.currentTimeMillis;


public class MessageBoardFragment extends Fragment implements View.OnClickListener {
    private DatabaseReference mThreadReference;
    //Adapter to handle data of list view
    FirebaseThreadViewHolder adapter;
    private FirebaseRecyclerAdapter mFirebaseAdapter;
    RecyclerView mThreadsRecyclerView;
    ArrayList<Thread> messages = new ArrayList<>();
    LinearLayoutManager layoutManager;
    RecyclerView lv;

    boolean commentPost = false; // false = new post, true = comment an existing post
    int postToComment = 0;

    @Bind(R.id.submitCommentButton)
    Button submitCommentButton;
    View mainView;
    View subView;
    LayoutInflater inflater;
    ViewGroup container;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        System.out.println("MainView"+mainView);
        if (mainView == null) {
            final FragmentActivity c = getActivity();
            layoutManager = new LinearLayoutManager(c);
            this.inflater = inflater;
            this.container = container;
            mainView = inflater.inflate(R.layout.activity_message_board, container, false);
            LinearLayout commentLayout = (LinearLayout) mainView.findViewById(R.id.messageView);
            submitCommentButton = (Button) mainView.findViewById(R.id.submitCommentButton);
            submitCommentButton.setOnClickListener(this);


            /* The five different messages possible with the five possible persons */
            for (int i = 0; i < 6; i++) {
                commentLayout.addView(getThreadMessage(i,null));
            }

            /* Comment a post (the first one) */
            String[] comments = new String[]{"Awesome idea", "Cool. Lets do this", "I would love to join"};
            View v = getThreadMessage(6, comments[new Random().nextInt(2)]);
            v.setPadding(60,170,0,0);
            ((FrameLayout)commentLayout.getChildAt(0)).addView(v,1);

        }
        return mainView;
    }


    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submitCommentButton:
                EditText comment = (EditText) mainView.findViewById(R.id.commentEditText);

                /* Don't commit empty messages */
                if(comment.getText().toString().length() == 0){
                    Toast.makeText(getContext(), "Enter a message before submitting", Toast.LENGTH_LONG).show();
                }
                else {
                    LinearLayout mainLayOut = (LinearLayout) mainView.findViewById(R.id.messageView);
                    subView = inflater.inflate(R.layout.fragment_thread_detail, container, false);
                    TextView userName = (TextView) subView.findViewById(R.id.userNameTextView);
                    TextView message = (TextView) subView.findViewById(R.id.messageTextView);
                    ImageView profileimage = (ImageView) subView.findViewById(R.id.threadImageView);
                    profileimage.setBackgroundResource(R.drawable.profile_snow_2);
                    userName.setText("John");
                    message.setText(comment.getText());


                    if (!commentPost) { // Create a new post
                        mainLayOut.addView((FrameLayout) subView, 0);
                    } else { //Answer post
                        View v1 = setCommentMessage(comment.getText().toString());
                        /*To add several comments to same post */
                        int commentNbr = ((FrameLayout)mainLayOut.getChildAt(postToComment)).getChildCount();
                        v1.setPadding(60,180*commentNbr,0,0);
                        ((FrameLayout)mainLayOut.getChildAt(postToComment)).addView(v1,commentNbr);

                        commentPost = false;
                        if(postToComment == mainLayOut.getChildCount()-1) {
                            showNotification();
                        }
                    }
                    comment.setText("");
                }
                break;
        }
    }


    private View getThreadMessage(final int i, String newMessage){
        String[] userNames = new String[]{"David", "Lakmal", "Jonas", "Alex","Michela", "Rose", "Tom"};
        final String[] comments = new String[]{"Sunday is sunny, Let's do a hike",
                "How about a Ski weekend in Austria",
                "There is a mediaval festival in Zurich. Anyone traveling from Munich",
                "Me and my classmates are doing a trip to Venice next weekend",
                "Perfect weather next month. any ideas of a trip",
                "With my husband and our 3 children we would like to go to Black Forest on 02/19"};

        final LinearLayout commentLayout = (LinearLayout) mainView.findViewById(R.id.messageView);
        subView = inflater.inflate(R.layout.fragment_thread_detail, container, false);
        final TextView userName = (TextView)subView.findViewById(R.id.userNameTextView);
        final TextView message = (TextView)subView.findViewById(R.id.messageTextView);

        /* To comment a post */
        message.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                commentPost = true;
                switch (userName.getText().toString()){
                    case "Rose":
                        postToComment = commentLayout.getChildCount() -1;
                        Toast.makeText(getContext(), "Enter your comment for Rose", Toast.LENGTH_LONG).show();
                        break;
                    case "Michela":
                        postToComment = commentLayout.getChildCount() -2;
                        Toast.makeText(getContext(), "Enter your comment for Michela", Toast.LENGTH_LONG).show();
                        break;
                    case "Alex":
                        postToComment = commentLayout.getChildCount() -3;
                        Toast.makeText(getContext(), "Enter your comment for Alex", Toast.LENGTH_LONG).show();
                        break;
                    case "Jonas":
                        postToComment = commentLayout.getChildCount() -4;
                        Toast.makeText(getContext(), "Enter your comment for Jonas", Toast.LENGTH_LONG).show();
                        break;
                    case "Lakmal":
                        postToComment = commentLayout.getChildCount() -5;
                        Toast.makeText(getContext(), "Enter your comment for Lakmal", Toast.LENGTH_LONG).show();
                        break;
                    case "David":
                        postToComment = commentLayout.getChildCount() -6;
                        Toast.makeText(getContext(), "Enter your comment for David", Toast.LENGTH_LONG).show();
                        break;
                    case "Tom": // Already possible to comment with David's post
                        commentPost = false;
                        break;
                    default:
                        break;
                }
            }
        });


        ImageView profileimage = (ImageView)subView.findViewById(R.id.threadImageView);
        final TextView scoreText = (TextView)subView.findViewById(R.id.scoreTextView);
        Random ran = new Random();
        scoreText.setText(String.valueOf(ran.nextInt(11)));

        /* Update vote on click */
        ImageButton upScore = (ImageButton) subView.findViewById(R.id.upImageButton);
        upScore.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                String score = scoreText.getText().toString();
                scoreText.setText(String.valueOf(Integer.parseInt(score) + 1));

            }
        });


        int userNameIndex = i;
        int commentIndex = i;
        userName.setText(userNames[userNameIndex]);

        if (userNameIndex >2){
            profileimage.setBackgroundResource(R.drawable.robot1);
        }else{
            profileimage.setBackgroundResource(R.drawable.robot);
        }

        if(newMessage!=null){
            message.setText(newMessage);
            profileimage.setBackgroundResource(R.drawable.robot2);
        }else{
            message.setText(comments[commentIndex]);
        }
        return subView;
    }

    private View setCommentMessage(String comment){

        //final LinearLayout commentLayout = (LinearLayout) mainView.findViewById(R.id.messageView);
        subView = inflater.inflate(R.layout.fragment_thread_detail, container, false);

        final TextView userName = (TextView)subView.findViewById(R.id.userNameTextView);
        userName.setText("John");

        TextView message = (TextView)subView.findViewById(R.id.messageTextView);
        message.setText(comment);

        ImageView profileimage = (ImageView)subView.findViewById(R.id.threadImageView);
        profileimage.setBackgroundResource(R.drawable.profile_snow_2);

        final TextView scoreText = (TextView)subView.findViewById(R.id.scoreTextView);
        scoreText.setText(String.valueOf(0));
        return subView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    /* Show notification, used for scenario 2 */
    private void showNotification() {
        final Intent intent = new Intent("com.example.renttogether.fragments.MessageBoardFragment");
        final NotificationManager manager = (NotificationManager) getActivity().getSystemService(NOTIFICATION_SERVICE);
        final PendingIntent pendingIntent = PendingIntent.getActivity(getContext(), 1, intent, 0);
        final Notification.Builder builder = new Notification.Builder(getContext());

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                builder.setAutoCancel(false);
                builder.setTicker("RentTogether");
                builder.setContentTitle("RentTogether");
                builder.setContentText("Your trip to Black Forest is now available !");
                builder.setSmallIcon(R.drawable.ic_bus_white);
                builder.setContentIntent(pendingIntent);
                builder.setOngoing(true);
                builder.setSubText("Go to your suggested trips :)");   //API level 16
                builder.setNumber(100);
                builder.setWhen(currentTimeMillis()+10000);
                builder.build();
                builder.setPriority(Notification.PRIORITY_MAX);
                builder.setDefaults(Notification.DEFAULT_VIBRATE);
                Notification myNotication = builder.getNotification();
                manager.notify(11, myNotication);
            }
        }, 10000);

    }

}
