package com.example.renttogether.fragments;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.renttogether.database.DatabaseHandler;
import com.example.renttogether.R;
import com.example.renttogether.database.TripsDB;
import com.example.renttogether.activities.CustomListAdapter;
import com.example.renttogether.activities.TripResultActivity;

import java.util.ArrayList;


public class TrainFragment extends ListFragment {

    //List of all trips for day query
    ArrayList<TripsDB> resultTotalTrips = new ArrayList<>();

    //List of trips currently displayed
    ArrayList<TripsDB> resultTrips = new ArrayList<>();

    //Adapter to handle data of list view
    CustomListAdapter adapter;

    ListView lv;

    public TrainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_transport, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // save views as variables in this method
        // "view" is the one returned from onCreateView

        /* Define list view */
        lv = (ListView) view.findViewById(R.id.list);

        /* Get accurate trips from DB */
        DatabaseHandler db = new DatabaseHandler(getContext());
        resultTotalTrips = db.getTripByDepartureAndArrivalAndMeansOfTransportAndDate(TripResultActivity.getDeparturePoint(),
                TripResultActivity.getArrivalPoint(), TripResultActivity.getDateTrip(), "train");

        resultTrips = BusFragment.displayFourGoodTrips(resultTotalTrips, TripResultActivity.getTimeTrip());


        /* Set custom adapter */
        adapter = new CustomListAdapter(getContext(), resultTrips);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {
                TripsDB trip = (TripsDB) adapter.getItemAtPosition(position);
                String url = trip.getUrl();
                if(url!=null) {
                    Uri uri = Uri.parse(url); // missing 'http://' will cause crashed
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(getContext(), "Sorry, this trip doesn't seem to be available anymore...", Toast.LENGTH_LONG).show();
                }
            }
        });

        /* Previous button */
        Button previous_button = (Button) view.findViewById(R.id.previousTrips);
        previous_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if(resultTotalTrips.size() != 0) {
                    if (BusFragment.displayPreviousFourGoodTrips(resultTotalTrips, resultTrips).size() != 0) {
                        resultTrips = BusFragment.displayPreviousFourGoodTrips(resultTotalTrips, resultTrips);

                        adapter = new CustomListAdapter(getContext(), resultTrips);
                        lv.setAdapter(adapter);

                        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {
                                TripsDB trip = (TripsDB) adapter.getItemAtPosition(position);
                                String url = trip.getUrl();
                                if (url != null) {
                                    Uri uri = Uri.parse(url); // missing 'http://' will cause crashed
                                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                    startActivity(intent);
                                }
                            }
                        });
                    } else {
                        Toast.makeText(getContext(), "No earlier trip available", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getContext(), "No earlier trip available", Toast.LENGTH_LONG).show();
                }
            }
        });

        /* Next button */
        Button next_button = (Button) view.findViewById(R.id.nextTrips);
        next_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if(resultTotalTrips.size() != 0) {
                    if (BusFragment.displayNextFourGoodTrips(resultTotalTrips, resultTrips).size() != 0) {
                        resultTrips = BusFragment.displayNextFourGoodTrips(resultTotalTrips, resultTrips);

                        adapter = new CustomListAdapter(getContext(), resultTrips);
                        lv.setAdapter(adapter);

                        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {
                                TripsDB trip = (TripsDB) adapter.getItemAtPosition(position);
                                String url = trip.getUrl();
                                if (url != null) {
                                    Uri uri = Uri.parse(url); // missing 'http://' will cause crashed
                                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                    startActivity(intent);
                                }
                            }
                        });
                    } else {
                        Toast.makeText(getContext(), "No later trip available", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getContext(), "No later trip available", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    /* public void addItems(String text) {
        listItems.add(text);
        adapter.notifyDataSetChanged();
     }*/

    public void removeAllItems(ArrayList<String> listItems, ArrayAdapter adapter) {
        for(int i=0; i<listItems.size(); i++) {
            listItems.remove(i);
            adapter.notifyDataSetChanged();
        }
    }
}
