package com.example.renttogether.database;

/**
 * Created by Alexandra GAUDRON on 29/12/2016.
 */
public class TripsDB {

    //private variables
    private int id;
    private String departure;
    private String arrival;
    private String date;
    private String time;
    private String meansOfTransport;
    private String price;
    private String company;
    private String duration;
    private String connection;
    private String url;
    private String state; // can be "booked", "suggested", "stand-by" or "offer"
    private String nbrOfPeople; // seat left for offers and missing people for stand-by
    private String nbrOfBookedTickets;  // when the trip is booked

    // Empty constructor
    public TripsDB(){

    }
    // constructor
    public TripsDB(int id, String departure, String arrival, String date, String time, String meansOfTransport,
                   String price, String company, String duration, String connection, String url, String state,
                   String nbrOfPeople, String nbrOfBookedTickets){
        this.id = id;
        this.departure = departure;
        this.arrival = arrival;
        this.date = date;
        this.time = time;
        this.meansOfTransport = meansOfTransport;
        this.price = price;
        this.company = company;
        this.duration = duration;
        this.connection = connection;
        this.url = url;
        this.state = state;
        this.nbrOfPeople = nbrOfPeople;
        this.nbrOfBookedTickets = nbrOfBookedTickets;
    }

    // constructor
    public TripsDB(String departure, String arrival, String date, String time, String meansOfTransport,
                   String price, String company, String duration, String connection, String url,
                   String state, String nbrOfPeople, String nbrOfBookedTickets){
        this.departure = departure;
        this.arrival = arrival;
        this.date = date;
        this.time = time;
        this.meansOfTransport = meansOfTransport;
        this.price = price;
        this.company = company;
        this.duration = duration;
        this.connection = connection;
        this.url = url;
        this.state = state;
        this.nbrOfPeople = nbrOfPeople;
        this.nbrOfBookedTickets = nbrOfBookedTickets;
    }
    // getting ID
    public int getid(){
        return this.id;
    }

    // setting id
    public void setid(int id){
        this.id = id;
    }

    // getting departure
    public String getDeparture(){
        return this.departure;
    }

    // setting departure
    public void setDeparture(String departure){
        this.departure = departure;
    }

    // getting arrival
    public String getArrival(){
        return this.arrival;
    }

    // setting arrival
    public void setArrival(String arrival){
        this.arrival = arrival;
    }

    // getting date
    public String getDate(){
        return this.date;
    }

    // setting date
    public void setDate(String date){
        this.date = date;
    }

    // getting time
    public String getTime(){
        return this.time;
    }

    // setting time
    public void setTime(String time){
        this.time = time;
    }

    // getting means of transport
    public String getMeansOfTransport(){
        return this.meansOfTransport;
    }

    // setting means  od transport
    public void setMeansOfTransport(String meansOfTransport){
        this.meansOfTransport = meansOfTransport;
    }

    // getting price
    public String getPrice(){
        return this.price;
    }

    // setting price
    public void setPrice(String price){
        this.price = price;
    }

    // getting company
    public String getCompany(){
        return this.company;
    }

    // setting company
    public void setCompany(String company){
        this.company = company;
    }

    // getting duration
    public String getDuration(){
        return this.duration;
    }

    // setting duration
    public void setDuration(String duration){
        this.duration = duration;
    }

    //getting connection number
    public String getConnection(){
        return this.connection;
    }

    // setting connection number
    public void setConnection(String connection){
        this.connection = connection;
    }

    //getting url
    public String getUrl(){
        return this.url;
    }

    // setting url
    public void setUrl(String url){
        this.url = url;
    }

    // getting state
    public String getState(){
        return state;
    }

    // setting state
    public void setState(String state) {
        this.state = state;
    }

    // getting number of people
    public String getNbrOfPeople(){
        return nbrOfPeople;
    }

    // setting number of people
    public void setNbrOfPeople(String nbr) {
        this.nbrOfPeople = nbr;
    }

    // getting number of booked tickets
    public String getNbrOfBookedTickets(){
        return nbrOfBookedTickets;
    }

    // setting number of booked tickets
    public void setNbrOfBookedTickets(String nbrOfBookedTickets){
        this.nbrOfBookedTickets = nbrOfBookedTickets;
    }
}