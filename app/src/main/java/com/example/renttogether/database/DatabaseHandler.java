package com.example.renttogether.database;

/**
 * Created by Alexandra GAUDRON on 29/12/2016.
 */
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 11;

    // Database Name
    private static final String DATABASE_NAME = "tripsManager";

    // Trips table name
    private static final String TABLE_TRIPS = "trips";

    // Trips Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_DEPARTURE = "departure";
    private static final String KEY_ARRIVAL = "arrival";
    private static final String KEY_DATE = "date";
    private static final String KEY_TIME = "time";
    private static final String KEY_MEANS_OF_TRANSPORT = "meansOfTransport";
    private static final String KEY_PRICE = "price";
    private static final String KEY_COMPANY = "company";
    private static final String KEY_DURATION = "duration";
    private static final String KEY_CONNECTION = "connection";
    private static final String KEY_URL = "url";
    private static final String KEY_STATE = "state";  // can be "booked", "suggested", "stand-by" or "offer"
    private static final String KEY_NBR = "nbr";      // seat left for offers and missing people for stand-by
    private static final String KEY_TICKET = "ticket";  // when the trip is booked, how many tickets were bought

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TRIPS_TABLE = "CREATE TABLE " + TABLE_TRIPS + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_DEPARTURE + " TEXT," + KEY_ARRIVAL + " TEXT," + KEY_DATE + " TEXT," + KEY_TIME + " TEXT,"
                + KEY_MEANS_OF_TRANSPORT + " TEXT," + KEY_PRICE + " TEXT," + KEY_COMPANY + " TEXT," + KEY_DURATION
                + " TEXT," + KEY_CONNECTION + " TEXT," + KEY_URL + " TEXT," + KEY_STATE + " TEXT," + KEY_NBR + " TEXT,"
                + KEY_TICKET + " TEXT" + ")";
        db.execSQL(CREATE_TRIPS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRIPS);

        // Create tables again
        onCreate(db);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new trips
    public void addTrip(TripsDB trip) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_DEPARTURE, trip.getDeparture());
        values.put(KEY_ARRIVAL, trip.getArrival());
        values.put(KEY_DATE, trip.getDate());
        values.put(KEY_TIME, trip.getTime());
        values.put(KEY_MEANS_OF_TRANSPORT, trip.getMeansOfTransport());
        values.put(KEY_PRICE, trip.getPrice());
        values.put(KEY_COMPANY, trip.getCompany());
        values.put(KEY_DURATION, trip.getDuration());
        values.put(KEY_CONNECTION, trip.getConnection());
        values.put(KEY_URL, trip.getUrl());
        values.put(KEY_STATE, trip.getState());
        values.put(KEY_NBR, trip.getNbrOfPeople());
        values.put(KEY_TICKET, trip.getNbrOfBookedTickets());

        // Inserting Row
        db.insert(TABLE_TRIPS, null, values);
        db.close(); // Closing database connection
    }

    // Getting single trip by ID
    public TripsDB getTripByID(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_TRIPS,
                new String[] { KEY_ID, KEY_DEPARTURE, KEY_ARRIVAL, KEY_DATE, KEY_TIME, KEY_MEANS_OF_TRANSPORT, KEY_PRICE,
                        KEY_COMPANY, KEY_DURATION, KEY_CONNECTION, KEY_URL, KEY_STATE, KEY_NBR, KEY_TICKET}, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        TripsDB trip = new TripsDB(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4),
                cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8),
                cursor.getString(9), cursor.getString(10), cursor.getString(11),
                cursor.getString(12), cursor.getString(13));
        cursor.close();
        return trip;
    }

    // Getting single trip by all criteria
    public ArrayList<TripsDB> getTripByDepartureAndArrivalAndMeansOfTransportAndDate(String departure, String arrival, String date,
                                                                                     String meansOfTransport) {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<TripsDB> res = new ArrayList<>();
        Cursor cursor;

        if(departure.equals("whatever") && arrival.equals("whatever")){ //Random departure and arrival points
            cursor = db.query(TABLE_TRIPS,
                    new String[] { KEY_ID, KEY_DEPARTURE, KEY_ARRIVAL, KEY_DATE, KEY_TIME, KEY_MEANS_OF_TRANSPORT, KEY_PRICE,
                            KEY_COMPANY, KEY_DURATION, KEY_CONNECTION, KEY_URL, KEY_STATE, KEY_NBR, KEY_TICKET},
                            KEY_DATE + "=? AND " + KEY_MEANS_OF_TRANSPORT +"=?",
                    new String[] { date, meansOfTransport}, null, null, null, null);
        } else {
            if(departure.equals("whatever")){ // Random departure point
                cursor = db.query(TABLE_TRIPS,
                        new String[] { KEY_ID, KEY_DEPARTURE, KEY_ARRIVAL, KEY_DATE, KEY_TIME, KEY_MEANS_OF_TRANSPORT, KEY_PRICE,
                                KEY_COMPANY, KEY_DURATION, KEY_CONNECTION, KEY_URL, KEY_STATE, KEY_NBR, KEY_TICKET},
                                KEY_ARRIVAL + "=? AND " + KEY_DATE + "=? AND " + KEY_MEANS_OF_TRANSPORT +"=?",
                        new String[] { arrival, date, meansOfTransport}, null, null, null, null);
            } else { // Random arrival point
                if(arrival.equals("whatever")) {
                    cursor = db.query(TABLE_TRIPS,
                            new String[] { KEY_ID, KEY_DEPARTURE, KEY_ARRIVAL, KEY_DATE, KEY_TIME, KEY_MEANS_OF_TRANSPORT, KEY_PRICE,
                                    KEY_COMPANY, KEY_DURATION, KEY_CONNECTION, KEY_URL, KEY_STATE, KEY_NBR, KEY_TICKET},
                            KEY_DEPARTURE + "=? AND " + KEY_DATE + "=? AND " + KEY_MEANS_OF_TRANSPORT +"=?",
                            new String[] { departure, date, meansOfTransport}, null, null, null, null);
                } else { // Selected departure and arrival points
                    cursor = db.query(TABLE_TRIPS,
                            new String[] { KEY_ID, KEY_DEPARTURE, KEY_ARRIVAL, KEY_DATE, KEY_TIME, KEY_MEANS_OF_TRANSPORT, KEY_PRICE,
                                    KEY_COMPANY, KEY_DURATION, KEY_CONNECTION, KEY_URL, KEY_STATE, KEY_NBR, KEY_TICKET},
                            KEY_DEPARTURE + "=? AND " + KEY_ARRIVAL + "=? AND " + KEY_DATE + "=? AND "
                                    + KEY_MEANS_OF_TRANSPORT +"=?",
                            new String[] { departure, arrival, date, meansOfTransport}, null, null, null, null);
                }
            }
        }

        while (cursor.moveToNext()) {
            TripsDB trip = new TripsDB(Integer.parseInt(cursor.getString(0)),
                    cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4),
                    cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8),
                    cursor.getString(9), cursor.getString(10), cursor.getString(11),
                    cursor.getString(12), cursor.getString(13));

            /*Stand-by trips are not available */
            try {
                if(trip.getState().equals("stand-by")){
                }
                else {
                    res.add(trip);
                }
            } catch (NullPointerException err){
                res.add(trip);
                err.printStackTrace();
            }
        }
        cursor.close();
        //res = getTripsGoodTime(res, time);
        //res = displayFourGoodTrips(res, time);

        //Log.d("TAG", tripsToString(getAllTrips()));

        return res;
    }

    // Getting all booked trips
    public ArrayList<TripsDB> getTripsByState(String state) {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<TripsDB> res = new ArrayList<>();

        Cursor cursor = db.query(TABLE_TRIPS,
                new String[] { KEY_ID, KEY_DEPARTURE, KEY_ARRIVAL, KEY_DATE, KEY_TIME, KEY_MEANS_OF_TRANSPORT, KEY_PRICE,
                        KEY_COMPANY, KEY_DURATION, KEY_CONNECTION, KEY_URL, KEY_STATE, KEY_NBR, KEY_TICKET}, KEY_STATE + "=?",
                new String[] { state }, null, null, null, null);
        while (cursor.moveToNext()) {
            TripsDB trip = new TripsDB(Integer.parseInt(cursor.getString(0)),
                    cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4),
                    cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8),
                    cursor.getString(9), cursor.getString(10), cursor.getString(11),
                    cursor.getString(12), cursor.getString(13));
            res.add(trip);
        }
        cursor.close();

        return res;
    }

    // Getting All trips
    public ArrayList<TripsDB> getAllTrips() {
        ArrayList<TripsDB> tripList = new ArrayList<TripsDB>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_TRIPS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                TripsDB trip = new TripsDB();
                trip.setid(Integer.parseInt(cursor.getString(0)));
                trip.setDeparture(cursor.getString(1));
                trip.setArrival(cursor.getString(2));
                trip.setDate(cursor.getString(3));
                trip.setTime(cursor.getString(4));
                trip.setMeansOfTransport(cursor.getString(5));
                trip.setPrice(cursor.getString(6));
                trip.setCompany(cursor.getString(7));
                trip.setDuration(cursor.getString(8));
                trip.setConnection(cursor.getString(9));
                trip.setUrl(cursor.getString(10));
                trip.setState(cursor.getString(11));
                trip.setNbrOfPeople(cursor.getString(12));
                trip.setNbrOfBookedTickets(cursor.getString(13));

                // Adding contact to list
                tripList.add(trip);
            } while (cursor.moveToNext());
        }
        cursor.close();
        // return contact list
        return tripList;
    }

    // Updating single trip
    public int updateTrip(TripsDB trip) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_DEPARTURE, trip.getDeparture());
        values.put(KEY_ARRIVAL, trip.getArrival());
        values.put(KEY_DATE, trip.getDate());
        values.put(KEY_TIME, trip.getTime());
        values.put(KEY_MEANS_OF_TRANSPORT, trip.getMeansOfTransport());
        values.put(KEY_PRICE, trip.getPrice());
        values.put(KEY_COMPANY, trip.getCompany());
        values.put(KEY_DURATION, trip.getDuration());
        values.put(KEY_CONNECTION, trip.getConnection());
        values.put(KEY_URL, trip.getUrl());
        values.put(KEY_STATE, trip.getState());
        values.put(KEY_NBR, trip.getNbrOfPeople());
        values.put(KEY_TICKET, trip.getNbrOfBookedTickets());

        // updating row
        return db.update(TABLE_TRIPS, values, KEY_ID + " = ?",
                new String[] { String.valueOf(trip.getid()) });
    }

    // Deleting single trip
    public void deleteTrip(TripsDB trip) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_TRIPS, KEY_ID + " = ?",
                new String[] { String.valueOf(trip.getid()) });
        db.close();
    }

    // Deleting single trip
    public void deleteTripByID(int ID) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_TRIPS, KEY_ID + " = ?",
                new String[] { String.valueOf(ID) });
        db.close();
    }

    // Getting trips Count
    public int getTripsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_TRIPS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int res = cursor.getCount();
        cursor.close();

        // return count
        return res;
    }

    public static String tripsToString(ArrayList<TripsDB> trips){
        String res = "";
        for(TripsDB trip : trips) {
            res += "Id: "+trip.getid()+", Departure: " + trip.getDeparture() + ", Arrival: " + trip.getArrival()
                    + ", Date: " + trip.getDate() + ", Time: " + trip.getTime()
                    + ", Means of transport: " + trip.getMeansOfTransport() + ", Price: "
                    + trip.getPrice() + ", Company: " + trip.getCompany() + ", Duration: " + trip.getDuration()
                    + ", Connection: " + trip.getConnection() + ", Url: " + trip.getUrl()
                    + ", State:" + trip.getState() + ", Nbr: "
                    + trip.getNbrOfPeople() +  ", Tickets: " + trip.getNbrOfBookedTickets() + "\n\n";
        }
        return res;
    }

    /*Return trips scheduled on time or after time det by user */
    /*public ArrayList<TripsDB> getTripsGoodTime(ArrayList<TripsDB> trips, String time_str){
        ArrayList<TripsDB> res = new ArrayList<>();
        int time_int = timeStrToInt(time_str);
        for(TripsDB trip : trips){
            if(timeStrToInt(trip.getTime()) >= time_int){
                res.add(trip);
            }

            res = orderArray(res);
        }
        return res;
    }*/
}