package com.example.renttogether.activities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.renttogether.R;
import com.example.renttogether.database.TripsDB;

import java.util.ArrayList;

/**
 * Created by Alexandra GAUDRON on 01/01/2017.
 */
public class CustomListAdapterBookedTrips extends BaseAdapter {
    private ArrayList<TripsDB> listData;
    private LayoutInflater layoutInflater;

    public CustomListAdapterBookedTrips(Context aContext, ArrayList<TripsDB> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(aContext);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_row_booked_trips_layout, null);
            holder = new ViewHolder();
            holder.companyView = (TextView) convertView.findViewById(R.id.company);
            holder.departureAndArrivalView = (TextView) convertView.findViewById(R.id.departureAndArrival);
            holder.dateView = (TextView) convertView.findViewById(R.id.date);
            holder.timeView = (TextView) convertView.findViewById(R.id.time);
            holder.ticketView = (TextView) convertView.findViewById(R.id.ticket);
            holder.meansOfTransportView = (TextView) convertView.findViewById(R.id.meansOfTransport);
            holder.durationView = (TextView) convertView.findViewById(R.id.duration);
            holder.connectionView = (TextView) convertView.findViewById(R.id.connection);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.companyView.setText("" + listData.get(position).getCompany());
        holder.departureAndArrivalView.setText("" + listData.get(position).getDeparture() + " - " + listData.get(position).getArrival());
        holder.dateView.setText("On " + listData.get(position).getDate());
        holder.timeView.setText("Departure: " + listData.get(position).getTime());
        holder.ticketView.setText("" + listData.get(position).getNbrOfBookedTickets() + " Ticket(s)");
        holder.meansOfTransportView.setText("By " + listData.get(position).getMeansOfTransport());
        holder.durationView.setText("Duration " + listData.get(position).getDuration());
        holder.connectionView.setText("" + listData.get(position).getConnection());
        return convertView;
    }

    private static class ViewHolder {
        TextView companyView;
        TextView departureAndArrivalView;
        TextView dateView;
        TextView timeView;
        TextView ticketView;
        TextView meansOfTransportView;
        TextView durationView;
        TextView connectionView;
    }
}