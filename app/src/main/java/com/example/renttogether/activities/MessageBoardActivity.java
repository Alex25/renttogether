package com.example.renttogether.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.renttogether.R;
import com.example.renttogether.database.Thread;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;

import butterknife.Bind;

public class MessageBoardActivity extends AppCompatActivity implements View.OnClickListener {

    private DatabaseReference mThreadReference;

    @Bind(R.id.submitCommentButton) Button submitCommentButton;

    public ArrayList<Thread> mThreads = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LinearLayout messageBoardLayOut = (LinearLayout)findViewById(R.id.messageBoardLayout);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        params.gravity = Gravity.CENTER_VERTICAL;

        submitCommentButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == submitCommentButton) {

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
