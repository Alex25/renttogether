package com.example.renttogether.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.renttogether.Constants;
import com.example.renttogether.R;
import com.example.renttogether.activities.ThreadDetailActivity;
import com.example.renttogether.database.Thread;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.parceler.Parcels;

import java.util.ArrayList;

public class FirebaseThreadViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    View mView;
    Context mContext;

    public FirebaseThreadViewHolder(View itemView) {
        super(itemView);
        mView = itemView;
        mContext = itemView.getContext();
        itemView.setOnClickListener(this);
    }

    public void bindThread(Thread thread) {

        TextView userNameTextView = (TextView) mView.findViewById(R.id.userNameTextView);
        TextView messageTextView = (TextView) mView.findViewById(R.id.messageTextView);
        TextView scoreTextView = (TextView) mView.findViewById(R.id.scoreTextView);
        ImageButton upImageButton = (ImageButton) mView.findViewById(R.id.upImageButton);
        ImageButton downImageButton = (ImageButton) mView.findViewById(R.id.downImageButton);

        messageTextView.setText(thread.getMessage());
        userNameTextView.setText(thread.getUserName());
        scoreTextView.setText(thread.getScore() + "");
    }

    @Override
    public void onClick(View v) {
        final ArrayList<Thread> threads = new ArrayList<>();

    }

}
