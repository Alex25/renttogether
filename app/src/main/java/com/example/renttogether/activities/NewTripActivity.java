package com.example.renttogether.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.CursorIndexOutOfBoundsException;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.renttogether.R;
import com.example.renttogether.database.DatabaseHandler;
import com.example.renttogether.database.TripsDB;
import com.example.renttogether.fragments.MessageBoardFragment;
import com.example.renttogether.fragments.NewTripFragment;
import com.example.renttogether.fragments.OffersFragment;
import com.example.renttogether.fragments.ProfileFragment;
import com.example.renttogether.fragments.UserTripsFragment;


/**
 * main activity
 */
public class NewTripActivity extends AppCompatActivity  {

    //Defining Variables
    private DrawerLayout drawerLayout;
    NavigationView navigationView;
    MessageBoardFragment messageBoardFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_trip);

        // Initializing Toolbar and setting it as the actionbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);;

        //Initializing NavigationView
        navigationView = (NavigationView) findViewById(R.id.nvView);
        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }


        // Initializing Drawer Layout and ActionBarToggle
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.openDrawer, R.string.app_name);

        //Setting the actionbarToggle to drawer layout
        drawerLayout.addDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();

        if (savedInstanceState == null) {
            selectDrawerItem(navigationView.getMenu().findItem(R.id.new_trip));
        }

        /* After payment display BookedTripsFragment */
        try {
            Intent intent = getIntent();
            final Bundle extras = intent.getExtras();
            if (extras.getString("PAYMENT").equals("yes")) {
                selectDrawerItem(navigationView.getMenu().findItem(R.id.current_trips));
            }
        }catch (RuntimeException err){
            err.printStackTrace();
        }



        /* Test if DB empty, if yes fill it */
        DatabaseHandler db = new DatabaseHandler(this);

        /*for(int i=1; i<17; i++) {
            db.deleteTripByID(i);
        }*/

        try {
            db.getTripByID(1);
            Log.d("TAG", "DB not empty");
        }
        catch (CursorIndexOutOfBoundsException err) {
            Log.d("TAG", "DB empty");
            err.printStackTrace();

            // Time of departure MUST be with TWO digits for hour and TWO digits for minutes !!
            db.addTrip(new TripsDB("Berlin", "Munich", "02/16/2017", "13h00", "bus", "19€", "Flixbus", "7h05", "Direct",
                    "https://shop.flixbus.fr/search?adults=1&children=0&bikes=0&departureStation=&arrivalStation=&departureCity=88&arrivalCity=94&rideDate=16.02.2017&oneway=&backRideDate=15.01.2017&adults=&children=&bikes=&_locale=fr&form_build_id=form-gcGt9EeOrC32_T9-7Nc3E8-RDwWhQo3J_RPLPqcFHGE&form_id=fb_search_form_main&_ga=1.130414671.1592071804.1483380721",
                    null, null, "0"));

            db.addTrip(new TripsDB("Berlin", "Munich", "02/16/2017", "10h30", "bus", "19€", "RentTogether", "7h10", "Direct", null, null, null, "0"));


            db.addTrip(new TripsDB("Berlin", "Munich", "02/16/2017", "07h30", "train", "29€", "DB", "6h46", "Direct",
                    "https://reiseauskunft.bahn.de/bin/query2.exe/dn?ld=15046&protocol=https:&seqnr=10&ident=k4.01441246.1484084897&rt=1&rememberSortType=minDeparture&sTID=C0-3.0@1&oCID=C0-3&orderSOP=yes&showAvail=yes&completeFulfillment=1&hafasSessionExpires=1001172314&zielorth=Muenchen&zielorte=Muenchen&zielortb=muenchen&zielorta=DEU&xcoorda=11558339&ycoorda=48140229&distancea=504&zielortm=M%FCnchen%20Hbf&services=hebma&bcrvglpreis=13600&HWAI=SELCON!lastsel=C0-3!&",
                    null, null, "0"));

            db.addTrip(new TripsDB("Berlin", "Munich", "02/16/2017", "09h30", "train", "29€", "DB", "6h11", "1 connection",
                    "https://reiseauskunft.bahn.de/bin/query2.exe/dn?ld=15046&protocol=https:&seqnr=10&ident=k4.01441246.1484084897&rt=1&rememberSortType=minDeparture&sTID=C1-2.0@1&oCID=C1-2&orderSOP=yes&showAvail=yes&completeFulfillment=1&hafasSessionExpires=1001172314&zielorth=Muenchen&zielorte=Muenchen&zielortb=muenchen&zielorta=DEU&xcoorda=11558339&ycoorda=48140229&distancea=504&zielortm=M%FCnchen%20Hbf&services=hebma&bcrvglpreis=13600&HWAI=SELCON!lastsel=C1-2!&",
                    null, null, "0"));

            db.addTrip(new TripsDB("Hamburg", "Frankfurt", "02/22/2017", "11h00", "plane", "107,36€", "Lufthansa", "1h10", "Direct",
                    "https://book.lufthansa.com/lh/dyn/air-lh/revenue/viewFlights", null, null, "0"));

            db.addTrip(new TripsDB("Berlin", "Munich", "02/16/2017", "19h00", "bus", "22€", "Flixbus", "11h00", "Direct",
                    "https://shop.flixbus.fr/search?route=Berlin-Munich&rideDate=16.02.2017&adults=1&children=0&bikes=0&departureCity=88&departureStation=&arrivalCity=94&arrivalStation=&currency=EUR",
                    null, null, "0"));

            db.addTrip(new TripsDB("Berlin", "Munich", "02/16/2017", "12h00", "bus", "22€", "Flixbus", "8h15", "Direct",
                    "https://shop.flixbus.fr/search?route=Berlin-Munich&rideDate=16.02.2017&adults=1&children=0&bikes=0&departureCity=88&departureStation=&arrivalCity=94&arrivalStation=&currency=EUR",
                    null, null, "0"));

            db.addTrip(new TripsDB("Berlin", "Munich", "02/16/2017", "09h10", "bus", "34€", "Flixbus", "10h25", "1 connection",
                    "https://shop.flixbus.fr/search?route=Berlin-Munich&rideDate=16.02.2017&adults=1&children=0&bikes=0&departureCity=88&departureStation=&arrivalCity=94&arrivalStation=&currency=EUR",
                    null,  null, "0"));

            db.addTrip(new TripsDB("Berlin", "Munich", "02/03/2017", "09h10", "bus", "34€", "Flixbus", "10h25", "1 connection", null, null, null, "0"));

            db.addTrip(new TripsDB("Berlin", "Munich", "02/16/2017", "06h27", "train", "79.90€", "DB", "6h12", "1 connection",
                    "https://reiseauskunft.bahn.de/bin/query2.exe/dn?ld=15046&protocol=https:&seqnr=2&ident=k4.01441246.1484084897&rt=1&rememberSortType=minDeparture&sTID=C0-0.0@1&oCID=C0-0&orderSOP=yes&showAvail=yes&completeFulfillment=1&hafasSessionExpires=1001172308&zielorth=Muenchen&zielorte=Muenchen&zielortb=muenchen&zielorta=DEU&xcoorda=11558339&ycoorda=48140229&distancea=504&zielortm=M%FCnchen%20Hbf&services=hebma&bcrvglpreis=13600&HWAI=SELCON!lastsel=C0-0!&",
                    null, null, "0"));

            db.addTrip(new TripsDB("Berlin", "Munich", "02/16/2017", "06h27", "train", "79.90€", "DB", "6h16", "Direct",
                    "https://reiseauskunft.bahn.de/bin/query2.exe/dn?ld=15046&protocol=https:&seqnr=2&ident=k4.01441246.1484084897&rt=1&rememberSortType=minDeparture&sTID=C0-1.0@1&oCID=C0-1&orderSOP=yes&showAvail=yes&completeFulfillment=1&hafasSessionExpires=1001172308&zielorth=Muenchen&zielorte=Muenchen&zielortb=muenchen&zielorta=DEU&xcoorda=11558339&ycoorda=48140229&distancea=504&zielortm=M%FCnchen%20Hbf&services=hebma&bcrvglpreis=13600&HWAI=SELCON!lastsel=C0-1!&",
                    null, null, "0"));

            db.addTrip(new TripsDB("Berlin", "Munich", "02/16/2017", "07h30", "train", "29€", "DB", "6h09", "1 connection",
                    "https://reiseauskunft.bahn.de/bin/query2.exe/dn?ld=15046&protocol=https:&seqnr=7&ident=k4.01441246.1484084897&rt=1&rememberSortType=minDeparture&sTID=C0-3.0@1&oCID=C0-3&orderSOP=yes&showAvail=yes&completeFulfillment=1&hafasSessionExpires=1001172314&zielorth=Muenchen&zielorte=Muenchen&zielortb=muenchen&zielorta=DEU&xcoorda=11558339&ycoorda=48140229&distancea=504&zielortm=M%FCnchen%20Hbf&services=hebma&bcrvglpreis=13600&HWAI=SELCON!lastsel=C0-3!&",
                    null, null, "0"));

            db.addTrip(new TripsDB("Berlin", "Munich", "02/16/2017", "08h28", "train", "29€", "DB", "6h13", "1 connection",
                    "https://reiseauskunft.bahn.de/bin/query2.exe/dn?ld=15046&protocol=https:&seqnr=10&ident=k4.01441246.1484084897&rt=1&rememberSortType=minDeparture&sTID=C1-0.0@1&oCID=C1-0&orderSOP=yes&showAvail=yes&completeFulfillment=1&hafasSessionExpires=1001172314&zielorth=Muenchen&zielorte=Muenchen&zielortb=muenchen&zielorta=DEU&xcoorda=11558339&ycoorda=48140229&distancea=504&zielortm=M%FCnchen%20Hbf&services=hebma&bcrvglpreis=13600&HWAI=SELCON!lastsel=C1-0!&",
                    null, null, "0"));

            db.addTrip(new TripsDB("Berlin", "Munich", "02/16/2017", "08h28", "train", "29€", "DB", "6h18", "1 connection",
                    "https://reiseauskunft.bahn.de/bin/query2.exe/dn?ld=15046&protocol=https:&seqnr=10&ident=k4.01441246.1484084897&rt=1&rememberSortType=minDeparture&sTID=C1-1.0@1&oCID=C1-1&orderSOP=yes&showAvail=yes&completeFulfillment=1&hafasSessionExpires=1001172314&zielorth=Muenchen&zielorte=Muenchen&zielortb=muenchen&zielorta=DEU&xcoorda=11558339&ycoorda=48140229&distancea=504&zielortm=M%FCnchen%20Hbf&services=hebma&bcrvglpreis=13600&HWAI=SELCON!lastsel=C1-1!&",
                    null, null, "0"));

            db.addTrip(new TripsDB("Berlin", "Munich", "02/16/2017", "09h30", "train", "29€", "DB", "6h36", "Direct",
                    "https://reiseauskunft.bahn.de/bin/query2.exe/dn?ld=15046&protocol=https:&seqnr=10&ident=k4.01441246.1484084897&rt=1&rememberSortType=minDeparture&sTID=C1-3.0@1&oCID=C1-3&orderSOP=yes&showAvail=yes&completeFulfillment=1&hafasSessionExpires=1001172314&zielorth=Muenchen&zielorte=Muenchen&zielortb=muenchen&zielorta=DEU&xcoorda=11558339&ycoorda=48140229&distancea=504&zielortm=M%FCnchen%20Hbf&services=hebma&bcrvglpreis=12150&HWAI=SELCON!lastsel=C1-3!&",
                    null, null, "0"));

            db.addTrip(new TripsDB("Berlin", "Munich", "02/16/2017", "10h27", "train", "105.90€", "DB", "6h11", "Direct",
                    "https://reiseauskunft.bahn.de/bin/query2.exe/dn?ld=15046&protocol=https:&seqnr=10&ident=k4.01441246.1484084897&rt=1&rememberSortType=minDeparture&sTID=C1-4.0@1&oCID=C1-4&orderSOP=yes&showAvail=yes&completeFulfillment=1&hafasSessionExpires=1001172314&zielorth=Muenchen&zielorte=Muenchen&zielortb=muenchen&zielorta=DEU&xcoorda=11558339&ycoorda=48140229&distancea=504&zielortm=M%FCnchen%20Hbf&services=hebma&bcrvglpreis=13600&HWAI=SELCON!lastsel=C1-4!&",
                    null, null, "0"));

            db.addTrip(new TripsDB("Munich", "Aachen", "02/09/2017", "11h35", "bus", "15€", "RentTogether", "9h50", "Direct", null, "offer", "3", "0"));

            db.addTrip(new TripsDB("Munich", "Garmisch-Partenkirchen", "02/18/2017", "08h00", "bus", "5€", "RentTogether", "1h15", "Direct", null, "stand-by", "6", "0"));

            db.addTrip(new TripsDB("Munich", "Black Forest", "02/19/2017", "10h25", "bus", "15€", "RentTogether", "3h45", "Direct", null, null, null, "0"));

            db.addTrip(new TripsDB("Munich", "Winterberg", "02/25/2017", "08h02", "bus", "17€", "RentTogether", "6h10", "Direct", null, "stand-by", "1", "0"));

            db.addTrip(new TripsDB("Munich", "Feldberg", "02/15/2017", "9h05", "bus", "15€", "RentTogether", "4h15", "Direct", null, "stand-by", "4", "0"));

            db.addTrip(new TripsDB("Berlin", "Munich", "02/22/2017", "09h55", "bus", "19€", "RentTogether", "7h10", "Direct", null, null, null, "0"));

            db.addTrip(new TripsDB("Berlin", "Munich", "02/16/2017", "09h20", "plane", "64.29€", "Airberline", "1h15", "Direct", "https://www.airberlin.com/fr/booking/flight/vacancy.php?sid=b3a20fdf49e605e83023&FlightBookingForm_TripType=OW&language_name=French_fr&site_edition=fr-FR&market=FR&bookingmask_widget_id=bookingmask-widget-stageoffer&bookingmask_widget_dateformat=",
                    null, null, "0"));

            db.addTrip(new TripsDB("Berlin", "Munich", "02/16/2017", "08h40 ", "plane", "44.00€", "Transavia", "1h15", "Direct", "https://www.transavia.com/fr-FR/reservez-un-vol/vols/rechercher/?ds=SXF&as=MUC&od=16&om=02&oy=2017&ap=1&cp=0&ip=0&utm_source=googleflights&utm_medium=metasearch&utm_campaign=SXFMUC",
                    null, null, "0"));

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer_view.
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {
        // Create a new fragment and specify the fragment to show based on nav item clicked
        Fragment fragment = null;
        Class fragmentClass;
        switch(menuItem.getItemId()) {
            case R.id.new_trip:
                fragmentClass = NewTripFragment.class;
                setTitle(R.string.app_name);
                break;
            case R.id.offers:
                fragmentClass = OffersFragment.class;
                setTitle(menuItem.getTitle());
                break;
            case R.id.profile:
                fragmentClass = ProfileFragment.class;
                setTitle(menuItem.getTitle());
                break;
            case R.id.current_trips:
                fragmentClass = UserTripsFragment.class;
                setTitle(menuItem.getTitle());
                break;
            case R.id.forum:
                fragmentClass = MessageBoardFragment.class;
                setTitle(menuItem.getTitle());

                /* Set trip as suggested (it's for scenario 2 when user post message, it should get a notification that his trip
                is now in "suggested") -> it's just to avoid the notification thing...
                 */
                DatabaseHandler db = new DatabaseHandler(getApplicationContext());
                TripsDB bookedTrip = db.getTripByID(19);
                if(bookedTrip.getState() == null) {
                    bookedTrip.setState("suggested");
                    db.updateTrip(bookedTrip);
                }

                break;
            case R.id.log_out:
                Toast.makeText(getApplicationContext(), "Log out", Toast.LENGTH_SHORT).show();
                drawerLayout.closeDrawers();
                return;
            default:
                fragmentClass = NewTripFragment.class;
                setTitle(R.string.app_name);
        }

        try {
            if (fragmentClass == MessageBoardFragment.class){
                if(messageBoardFragment==null){
                    messageBoardFragment = new MessageBoardFragment();
                }
                fragment = messageBoardFragment;
            }else{
                fragment = (Fragment) fragmentClass.newInstance();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

        // Highlight the selected item has been done by NavigationView
        menuItem.setChecked(true);
        // Set action bar title
        // setTitle(menuItem.getTitle());
        // Close the navigation drawer_view
        drawerLayout.closeDrawers();
    }

    @Override
    public void onBackPressed() {
        // Do nothing
    }
}
