package com.example.renttogether.activities;

/**
 * Created by rclakmal on 1/28/17.
 */

public class DataLoader {


    String[] numbers = {"Lakmal", "Alex", "David", "Chris", "Tom"};
    String[] statics = {"I would like to go to black forest this sunday", "Cool forum", "It might rain this weekend", "Ski weekend would be cool", "This is great"};



    public void setFirstdata(int position, String data){
        numbers[position]=data;
    }
    public void setSecondData(int position, String data){
        statics[position]=data;
    }

    public String[] getFirstdata(){
        return numbers;
    }
    public String[] getSeconddata(){
        return statics;
    }

}
