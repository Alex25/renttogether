package com.example.renttogether.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.renttogether.fragments.BusFragment;
import com.example.renttogether.fragments.PlaneFragment;
import com.example.renttogether.R;
import com.example.renttogether.fragments.TrainFragment;

import java.util.ArrayList;
import java.util.List;


public class TripResultActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private static String departure_point;
    private static String arrival_point;
    private static String date_trip;
    private static String time_trip;
    private static String nbr_of_people;

    private int[] tabIcons = {
            R.drawable.ic_bus_white,
            R.drawable.ic_train_white,
            R.drawable.ic_plane_white
    };

    static AlertDialog.Builder alertbox;
    static AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_result);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //getSupportActionBar().setTitle("test");
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        departure_point = extras.getString("DEPARTURE");
        arrival_point = extras.getString("ARRIVAL");
        date_trip = extras.getString("DATE");
        time_trip = extras.getString("TIME");
        nbr_of_people = extras.getString("NBR_SET");
        if(nbr_of_people.equals("Number of passengers")){
            nbr_of_people = "1";
        }
        TextView tv = (TextView) findViewById(R.id.toolbar_title);
        tv.setText(departure_point + " - " + arrival_point + "\n" + date_trip);

        showAlertDialog("Don't forget to...", "Have a look at the special offers and discuss your trip on the forum", this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new BusFragment(), "BUS");
        adapter.addFragment(new TrainFragment(), "TRAIN");
        adapter.addFragment(new PlaneFragment(), "PLANE");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<ListFragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public ListFragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(ListFragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
    }

    public static String getDeparturePoint(){
        return departure_point;
    }

    public static String getArrivalPoint(){
        return arrival_point;
    }

    public static String getDateTrip(){
        return date_trip;
    }

    public static String getTimeTrip(){
        return time_trip;
    }

    public static String getSetPeopleNbr(){
        return nbr_of_people;
    }

    /* Display or not the dialog for forum and special offers */
    public static void showAlertDialog(final String title, String message,
                                       final Context context) {
        if (alertDialog != null && alertDialog.isShowing()) {
            // A dialog is already open, wait for it to be dismissed, do nothing
        } else {
            alertbox = new AlertDialog.Builder(context);
            alertbox.setTitle(title);
            alertbox.setMessage(message);
            alertbox.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    alertDialog.dismiss();
                }
            });

            alertDialog = alertbox.create();
            alertDialog.show();
        }
    }
}