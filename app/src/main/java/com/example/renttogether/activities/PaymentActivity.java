package com.example.renttogether.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.renttogether.R;
import com.example.renttogether.database.DatabaseHandler;
import com.example.renttogether.database.TripsDB;

public class PaymentActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    final int sdk = android.os.Build.VERSION.SDK_INT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        /* text view for Total */
        TextView total_tv = (TextView) findViewById(R.id.total);
        Intent intent = getIntent();
        final Bundle extras = intent.getExtras();
        total_tv.setText("TOTAL: " + extras.getString("NBR") + "x" + extras.getString("PRICE"));

        /* Background picture settings */
        if(sdk > Build.VERSION_CODES.KITKAT) {  // Not sure of the version to put
            Drawable map_background = ContextCompat.getDrawable(this, R.drawable.road_background_light);
        }
        else {
            Drawable map_background = ContextCompat.getDrawable(this, R.drawable.road_background);
            map_background.setAlpha(30); // 0 full transparency, 255 full opacity
        }
        /* Spinners expiration date */
        Spinner expiration_month_spinner = (Spinner) findViewById(R.id.expirationMonth_spinner);
        Spinner expiration_year_spinner = (Spinner) findViewById(R.id.expirationYear_spinner);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapterMonth = ArrayAdapter.createFromResource(this,
                R.array.months_array, R.layout.my_spinner_item);
        ArrayAdapter<CharSequence> adapterYear = ArrayAdapter.createFromResource(this,
                R.array.years_array, R.layout.my_spinner_item);

        // Specify the layout to use when the list of choices appears
        adapterMonth.setDropDownViewResource(R.layout.my_spinner_dropdown_item);
        adapterYear.setDropDownViewResource(R.layout.my_spinner_dropdown_item);

        // Apply the adapter to the spinner
        expiration_month_spinner.setAdapter(adapterMonth);
        expiration_month_spinner.setOnItemSelectedListener(this);
        expiration_year_spinner.setAdapter(adapterYear);
        expiration_year_spinner.setOnItemSelectedListener(this);

        //Set default values not as first element
        expiration_month_spinner.setSelection(2);
        expiration_year_spinner.setSelection(2);


        Button pay_button = (Button) findViewById(R.id.pay);
        pay_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                /* Fake payment check */
                final ProgressDialog myPd_ring= ProgressDialog.show(PaymentActivity.this, "Please wait", "Checking transaction...", true);
                myPd_ring.setCancelable(false);
                myPd_ring.setOnDismissListener(new DialogInterface.OnDismissListener() {

                    @Override
                    /* Confirmation of payment dialog */
                    public void onDismiss(DialogInterface arg0) {
                        // TODO Auto-generated method stub
                        new AlertDialog.Builder(PaymentActivity.this)
                                .setTitle("Transaction successful")
                                .setMessage("Thank you :)\nYou will receive a receipt by email")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        /* Update trip' isReserved attribute in DB */
                                        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
                                        TripsDB bookedTrip = db.getTripByID(extras.getInt("ID"));
                                        bookedTrip.setState("booked");
                                        bookedTrip.setNbrOfBookedTickets(extras.getString("NBR"));
                                        db.updateTrip(bookedTrip);

                                        Intent i = new Intent(PaymentActivity.this, NewTripActivity.class);
                                        Bundle extras = new Bundle();
                                        extras.putString("PAYMENT", "yes");
                                        i.putExtras(extras);
                                        startActivity(i);
                                    }
                                })
                                .show()
                                .setCancelable(false);

                    }
                });


                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        try
                        {
                            Thread.sleep(2000);
                        }catch(Exception e){}
                        myPd_ring.dismiss();
                    }
                }).start();
            }
        });


    }


    /* Spinner methods */
    // Retrieve selected item
    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        /*switch(parent.getId()) {
            case R.id.departure_spinner:
                departure = parent.getItemAtPosition(pos).toString();
                Log.d("DEPARTURE: ", departure);
                break;
            case R.id.arrival_spinner:
                arrival = parent.getItemAtPosition(pos).toString();
                Log.d("ARRIVAL: ", departure);
                break;
            default:
                break;
        }*/
    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

}
