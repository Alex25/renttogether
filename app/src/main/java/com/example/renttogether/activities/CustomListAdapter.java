package com.example.renttogether.activities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.renttogether.R;
import com.example.renttogether.database.TripsDB;

import java.util.ArrayList;

/**
 * Created by Alexandra GAUDRON on 01/01/2017.
 */
public class CustomListAdapter extends BaseAdapter {
    private ArrayList<TripsDB> listData;
    private LayoutInflater layoutInflater;

    public CustomListAdapter(Context aContext, ArrayList<TripsDB> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(aContext);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_row_layout, null);
            holder = new ViewHolder();
            holder.companyView = (TextView) convertView.findViewById(R.id.company);
            holder.timeView = (TextView) convertView.findViewById(R.id.time);
            holder.priceView = (TextView) convertView.findViewById(R.id.price);
            holder.departureAndArrivalView = (TextView) convertView.findViewById(R.id.departureAndArrival);
            holder.durationView = (TextView) convertView.findViewById(R.id.duration);
            holder.connectionView = (TextView) convertView.findViewById(R.id.connection);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String setNbr = TripResultActivity.getSetPeopleNbr();
        if(setNbr.equals("Passengers number")){
            setNbr = "1";
        }
        holder.companyView.setText("" + listData.get(position).getCompany());
        holder.timeView.setText("Departure: " + listData.get(position).getTime());
        holder.priceView.setText("" + setNbr + "x" + listData.get(position).getPrice());
        holder.departureAndArrivalView.setText("" + listData.get(position).getDeparture() + " - " + listData.get(position).getArrival());
        holder.durationView.setText("Duration " + listData.get(position).getDuration());
        holder.connectionView.setText("" + listData.get(position).getConnection());
        return convertView;
    }

    private static class ViewHolder {
        TextView companyView;
        TextView timeView;
        TextView priceView;
        TextView departureAndArrivalView;
        TextView durationView;
        TextView connectionView;
    }
}